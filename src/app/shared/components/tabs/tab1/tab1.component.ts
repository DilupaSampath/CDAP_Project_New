import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-tab1',
  templateUrl: './tab1.component.html',
  styleUrls: ['./tab1.component.scss']
})

export class Tab1Component implements OnInit {
  buttonColor1: string = '#345465'; //Default Color
  buttonColor2: string = '#345465'; //Default Color
  buttonColor3: string = '#345465'; //Default Color
  buttonColor4: string = '#345465'; //Default Color
  click1 = false;
  click2 = false;
  click3 = false;
  click4 = false;
  constructor() { }

  ngOnInit() {
  }
  addEvent1(){
    if(this.click1){
    this.buttonColor1 = '#345465'; //desired Color
    this.click1 = !this.click1;

    console.log("clicked");
  }
  else{
    this.click1 = !this.click1;
    this.buttonColor1='#00c851';
    console.log("clicked not");
  }
    /*
    YOUR FUNCTION CODE
    */
    
    }
    addEvent2(){
      if(this.click2){
      this.buttonColor2 = '#345465'; //desired Color
      this.click2 = !this.click2;

    }
    else{
      this.click2 = !this.click2;
      this.buttonColor2='#00c851';

    }
      /*
      YOUR FUNCTION CODE
      */
      
      }
      addEvent3(){
        if(this.click3){
        this.buttonColor3 = '#345465'; //desired Color
        this.click3 = !this.click3;

      }
      else{
        this.click3 = !this.click3;
        this.buttonColor3='#00c851';

      }
        /*
        YOUR FUNCTION CODE
        */
        
        }
        addEvent4(){
          if(this.click4){
          this.buttonColor4 = '#345465'; //desired Color
          this.click4 = !this.click4;
      
        }
        else{
          this.click4 = !this.click4;
          this.buttonColor4='#00c851';

        }
          /*
          YOUR FUNCTION CODE
          */
          
          }
}
