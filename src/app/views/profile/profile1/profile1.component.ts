import { Component, OnInit } from '@angular/core';
import { ServiceDoctor } from 'app/services.Doctor';
interface Doctor {
  name: string;
  ward: string;
  assingDate: string;
  priority: string;
}

@Component({
  selector: 'app-profile1',
  templateUrl: './profile1.component.html',
  styleUrls: ['./profile1.component.scss']
})

export class Profile1Component implements OnInit {
doctors:Doctor[];  
name: any;
ward: any;
assingDate: any;
priority: any;
title:any;
buttonTitle:any;
indexDoctro:any;
removeIndex:number;
statusRemove=false;

  constructor(private doctorServices: ServiceDoctor) {
    this.doctors= doctorServices.doctors;
    this.title="New Doctor";
    this.buttonTitle="Assign Doctor";
   }

  ngOnInit() {
    this.doctors= this.doctorServices.doctors;
  }
  onCreate(){
     console.log("doctor added...!"+this.name);
    this.doctorServices.onCreateDocter(this.name,this.ward,this.assingDate,this.priority);
    console.log("doctor added...!");
  }
  onUpdate(){
this.doctorServices.onUpdateDoctor(this.indexDoctro,this.name,this.ward,this.assingDate,this.priority);
  }
  setInsetTitle(){
    this.title="New Doctor";
      }
  setToForm(index:any){
    this.indexDoctro=index;
    this.buttonTitle="Save Changes";
    this.title="Update Doctor";
    this.name=this.doctors[index].name;
    this.ward=this.doctors[index].ward;
    this.assingDate=this.doctors[index].assingDate;
    this.priority=this.doctors[index].priority;
    console.log(this.doctors[index]);

  }
  onRemove(){
    this.doctorServices.removeDoctor(this.removeIndex);

  }
  onRemoveIndex(index:number){
    this.removeIndex=index;
  }
  launch_toast() {
    var x = document.getElementById("toast")
    x.className = "show";
    setTimeout(function(){ x.className = x.className.replace("show", ""); }, 5000);
}
launch_toast2() {
  var x = document.getElementById("toast1")
  x.className = "show";
  setTimeout(function(){ x.className = x.className.replace("show", ""); }, 5000);
}

}
