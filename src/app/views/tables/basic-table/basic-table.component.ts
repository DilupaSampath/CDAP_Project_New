import { Component, OnInit } from '@angular/core';
import { ServiceDoctor } from 'app/services.Doctor';
interface Nurses {
  name: string;
  ward: string;
  assingDate: string;
  priority: string;
}
@Component({
  selector: 'app-basic-table',
  templateUrl: './basic-table.component.html',
  styleUrls: ['./basic-table.component.scss']
})
export class BasicTableComponent implements OnInit {
  nurses:Nurses[];  
  name: any;
  ward: any;
  assingDate: any;
  priority: any;
  title:any;
  buttonTitle:any;
  indexDoctro:any;
  removeIndex:number;
  statusRemove=false;
  
  constructor(private nursesServices: ServiceDoctor) {
    this.nurses= nursesServices.nurses;
    this.title="New Nurse";
    this.buttonTitle="Assign Nurse";
   }

  ngOnInit() {
  }
  onCreate(){
    console.log("Nurse added...!"+this.name);
   this.nursesServices.onCreateNurse(this.name,this.ward,this.assingDate,this.priority);
   console.log("Nurse added...!");
 }
 onUpdate(){
this.nursesServices.onUpdateNurse(this.indexDoctro,this.name,this.ward,this.assingDate,this.priority);
 }
 setInsetTitle(){
   this.title="New Nurse";
     }
 setToForm(index:any){
   this.indexDoctro=index;
   this.buttonTitle="Save Changes";
   this.title="Update Nurse";
   this.name=this.nurses[index].name;
   this.ward=this.nurses[index].ward;
   this.assingDate=this.nurses[index].assingDate;
   this.priority=this.nurses[index].priority;
   console.log(this.nurses[index]);

 }
 onRemove(){
   this.nursesServices.removeNurse(this.removeIndex);

 }
 onRemoveIndex(index:number){
   this.removeIndex=index;
 }
 launch_toast() {
   var x = document.getElementById("toast")
   x.className = "show";
   setTimeout(function(){ x.className = x.className.replace("show", ""); }, 5000);
}
launch_toast2() {
 var x = document.getElementById("toast1")
 x.className = "show";
 setTimeout(function(){ x.className = x.className.replace("show", ""); }, 5000);
}


}
